const userInput = document.querySelector('#user-input')
const checkBtn = document.querySelector('#check-btn')
const clearBtn = document.querySelector('#clear-btn')
const resultDiv = document.querySelector('#result-div')


const checkUserInput = () => {
  if(userInput.value === '') {
    resultDiv.innerText = "Please provide a phone number"
    return
  }

const phoneNumber = userInput.value

 const resultCheck = checkPhoneNumber(phoneNumber)
  resultDiv.textContent = resultCheck ?
      `Valid US number: ${phoneNumber}` :
      `Invalid US number: ${phoneNumber}`
}

const checkPhoneNumber = (numb) => {
 let phoneRegex = /^([+]?1[\s]?)?((?:[(](?:[2-9]1[02-9]|[2-9][02-8][0-9])[)][\s]?)|(?:(?:[2-9]1[02-9]|[2-9][02-8][0-9])[\s.-]?)){1}([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2}[\s.-]?){1}([0-9]{4}){1}$/;

 return phoneRegex.test(numb)

}

const initValue = () => {
  userInput.value = ''
  resultDiv.innerText = ''
}

clearBtn.addEventListener('click', initValue)
checkBtn.addEventListener('click', checkUserInput)